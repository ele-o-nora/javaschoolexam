package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        //I am not sure if two minuses in a row should be interpreted as an invalid input (as done here)
        //or as a subtraction of a negative number (as in Google search and macOS Spotlight)
        if (statement == null || statement.isEmpty() || statement.contains("--")) {
            return null;
        }
        String toCalc = openBrackets(statement);
        if (toCalc == null) {
            return null;
        }
        toCalc = addSpaces(toCalc);
        String[] tokens = toCalc.split(" +");
        double result;
        try {
            result = calculate(tokens);
        } catch (Exception e) {
            return null;
        }
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.HALF_UP);
        return df.format(result);
    }

    private String openBrackets(String s) {
        if (!s.contains("(") && !s.contains(")")) {
            return s;
        }
        StringBuilder sb = new StringBuilder();
        int bracketCount = 0;
        int firstInsideBrackets = 0;
        int firstOutsideBrackets = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                if (bracketCount == 0) {
                    firstInsideBrackets = i + 1;
                    sb.append(s.substring(firstOutsideBrackets, i));
                }
                bracketCount++;
            } else if (s.charAt(i) == ')') {
                if (bracketCount == 0) {
                    return null;
                }
                bracketCount--;
                if (bracketCount == 0) {
                    sb.append(evaluate(s.substring(firstInsideBrackets, i)));
                    firstOutsideBrackets = i + 1;
                }
            }
        }
        if (bracketCount != 0) {
            return null;
        }
        sb.append(s.substring(firstOutsideBrackets));
        return sb.toString();
    }

    private String addSpaces(String s) {
        StringBuilder sb = new StringBuilder(s.replace("+", " + ")
                .replace("/", " / ")
                .replace("*", " * ")
                .replace("-", " -"));
        for (int i = 0; i < sb.length(); i++) {
            if (sb.charAt(i) == '-') {
                if (i - 2 >= 0 && Character.isDigit(sb.charAt(i - 2))) {
                    sb.insert(i + 1, " ");
                }
            }
        }
        return sb.toString();
    }

    private double calculate(String[] tokens) {
        if (tokens.length == 3) {
            switch (tokens[1]) {
                case "+":
                    return Double.parseDouble(tokens[0]) + Double.parseDouble(tokens[2]);
                case "-":
                    return Double.parseDouble(tokens[0]) - Double.parseDouble(tokens[2]);
                case "*":
                    return Double.parseDouble(tokens[0]) * Double.parseDouble(tokens[2]);
                case "/":
                    if (Double.parseDouble(tokens[2]) == 0) {
                        throw new RuntimeException();
                    }
                    return Double.parseDouble(tokens[0]) / Double.parseDouble(tokens[2]);
                default:
                    throw new RuntimeException();
            }
        }
        String[] rest = new String[tokens.length - 2];
        System.arraycopy(tokens, 2, rest, 0, rest.length);
        switch (tokens[1]) {
            case "+":
                return Double.parseDouble(tokens[0]) + calculate(rest);
            case "-":
                for (int i = 1; i < rest.length; i += 2) {
                    if (rest[i].equals("+")) {
                        rest[i] = "-";
                        continue;
                    }
                    if (rest[i].equals("-")) {
                        rest[i] = "+";
                    }
                }
                return Double.parseDouble(tokens[0]) - calculate(rest);
            case "*":
                rest[0] = String.valueOf(Double.parseDouble(tokens[0]) * Double.parseDouble(tokens[2]));
                return calculate(rest);
            case "/":
                rest[0] = String.valueOf(Double.parseDouble(tokens[0]) / Double.parseDouble(tokens[2]));
                return calculate(rest);
            default:
                throw new RuntimeException();
        }
    }

}
