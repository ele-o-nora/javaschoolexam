package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.size() < 3 || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        int rows = 2;
        int curElements = 2;
        int index = 3;
        while (index < inputNumbers.size()) {
            curElements++;
            rows++;
            if (inputNumbers.size() - index < curElements || curElements > Integer.MAX_VALUE / 2) {
                throw new CannotBuildPyramidException();
            } else if (inputNumbers.size() - index == curElements) {
                break;
            } else {
                index += curElements;
            }
        }
        int elementsInRow = curElements * 2 - 1;
        int[][] result = new int[rows][elementsInRow];
        curElements = 1;
        index = 0;
        Collections.sort(inputNumbers);
        for (int i = 0; i < rows; i++) {
            int left = (elementsInRow - (curElements * 2 - 1)) / 2;
            for (int j = 0; j < curElements; j++) {
                result[i][left] = inputNumbers.get(index);
                index++;
                left +=2;
            }
            curElements++;
        }
        return result;
    }

}
